#include <stdio.h>
#include <ctype.h>
#include <string.h>

void checkValidity(FILE *fp, int size, FILE *newFP, FILE *errorFile)
{
	int i;
	int valid;
	char unknownValidity[BUFSIZ];  

	while(fgets(unknownValidity, sizeof(unknownValidity),fp) != NULL)
	{
		valid = 0;
		//If location is longer or shorter than 4 digits then move onto the next iteration of the while loop
		if(unknownValidity[size] != '\n'|| unknownValidity[size] == '\0')
		{
			fprintf(errorFile, "Too long or short :%s", unknownValidity);			
			continue; 
		}	

		for(i = 0; i < size; i++)
		{

			//Checks if the character isn't a digit, if so then set valid as 1 which means the location is invalid
			if(isdigit(unknownValidity[i]) == 0)
			{
				valid = 1; 
				break;
			}
		}
		// 0 = true, 1 = false
		if(valid == 0)
		{

			if(size == 12)
			{
				printf(unknownValidity);

			}else
			{
				fprintf(newFP, "%s",  unknownValidity);
			}
		}else
		{
			fprintf(errorFile, "Invalid data : %s", unknownValidity);
		}
	} 
}

int main(int argc, char *argv[])
{
	FILE *filePtr;
	FILE *newFilePtr; 
	FILE *errorFile;

	if(argc < 3)
	{
		printf("There are too few arguments provided\n");
		return 1;
	}

	//Make sure the right arguments are given 
	char *locations = "--locations";
	if(strcmp(argv[1],locations) != 0)
	{
		printf("You have bad arguments. Include --locations\n");
		return 1;
	}

	newFilePtr = fopen("valid_locations_task3.txt", "w+");
	filePtr = fopen(argv[2],"r");
	errorFile = fopen("errors.log", "a");

	if(filePtr == NULL || newFilePtr == NULL || errorFile == NULL)
	{
		perror("ERROR");
		return 1;
	}

	//Checks if the barcode is valid
	checkValidity(filePtr, 4, newFilePtr, errorFile);
	checkValidity(stdin, 12, NULL, errorFile);

	//Closes the files
	fclose(errorFile);
	fclose(filePtr);
	fclose(newFilePtr);

	return 0; 
}
