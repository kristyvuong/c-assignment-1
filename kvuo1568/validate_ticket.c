#include <stdio.h>
#include <string.h>
#include <ctype.h>

int checksum(char barcode[])
{
	int total = 0; 
	int i;	

	for(i = 0; i < 11; i++)
	{
		total += barcode[i] - '0';
	}

	if(total % 10 == barcode[i] - '0')
	{
		//Return true i.e. the barcode has the right checksum
		return 0;	
	}

	return 1;
}

void checkForInvalidData(FILE *fp, int size, FILE *newFP, FILE *errorFile)
{
	//Contains data to be checked 
	char unknownValidData[BUFSIZ];
	int i;
	int valid;

	while(fgets(unknownValidData, sizeof(unknownValidData), fp) != NULL)
	{
		valid = 0;

		//Checks if the unknown data is too long or too short
		if(unknownValidData[size] != '\n' || unknownValidData[size] == '\0')
		{
			fprintf(errorFile, "Location/barcode is too long or short: %s", unknownValidData);			
			continue;
		}

		for(i = 0; i < size; i++)
		{
			if(isdigit(unknownValidData[i]) == 0)
			{
				//The data is invalid since i is not a digit (0-9)		
				valid = 1;
				break;
			}
		}

		if(valid == 0)
		{
			//Check if the barcode has the right checksum and then write to a file
			if(size == 12 && checksum(unknownValidData) == 0)
			{		
				fprintf(newFP, "%s", unknownValidData);
			}
			else if(size == 4)
			{
				fprintf(newFP, "%s", unknownValidData);  		
			}
		}else
		{
			fprintf(errorFile, "Invalid location/barcode: %s", unknownValidData);
		}
	}
	return;
}


void checkIfValidBarcodeLocation(FILE *barcodeFile, FILE *locationFile, FILE *errorFile)
{
	char barcode[14];
	char location[6];
	char simpleLocation[5];
	char barcodeLocation[5];
	char *simpleLocationPtr = &simpleLocation[0];
	char *barcodeLocationPtr = &barcodeLocation[0];
	//0 = valid, 1 = invalid
	int valid;

	//Sets both files back to the start 
	rewind(barcodeFile);
	rewind(locationFile);

	while(fgets(barcode, sizeof(barcode), barcodeFile) != NULL)
	{
		valid = 1; 

		//Copies the first 4 chars of barcode
		strncpy(barcodeLocationPtr, barcode, 4);
		while(fgets(location, sizeof(location), locationFile) != NULL)
		{
			//Copies the first 4 chars of the location i.e. without the /n and /0
			strncpy(simpleLocationPtr, location, 4);

			//Compares the two strings
			if(strcmp(simpleLocationPtr, barcodeLocationPtr) == 0)
			{
				printf(barcode);
				valid = 0;		
				break; 
			}
		}
		if(valid == 1)
		{ 
			fprintf(errorFile, "Invalid barcode location: %s", barcode);
		}
		rewind(locationFile);
	}
}

int main(int argc, char *argv[])
{
	FILE *fileLocationPtr;
	FILE *validLocationFilePtr;
	FILE *validBarcodeFilePtr;
	FILE *errorFile;
	//Checks if there are enough arguments
	if(argc < 3)
	{
		printf("There are too few arguments\n");
		return 1; 
	}

	//Make sure the right arguments are given 
	char *locations = "--locations";
	if(strcmp(argv[1],locations) != 0)
	{
		printf("You have bad arguments. Include --locations\n");
		return 1;
	}

	//Opens the files to read and be written on
	fileLocationPtr = fopen(argv[2], "r");
	validLocationFilePtr = fopen("validLocation.txt", "w+");
	validBarcodeFilePtr = fopen("validBarcodes.txt", "w+");
	errorFile = fopen("errors.log", "a");

	if(fileLocationPtr == NULL || validLocationFilePtr == NULL || validBarcodeFilePtr == NULL || errorFile == NULL)
	{
		perror("ERROR");
		return 1;
	}

	//Checks if the data is valid i.e. only digits and up to a certain length : 4 for location and 12 for barcode 
	checkForInvalidData(fileLocationPtr, 4, validLocationFilePtr, errorFile);
	checkForInvalidData(stdin, 12, validBarcodeFilePtr, errorFile);
	checkIfValidBarcodeLocation(validBarcodeFilePtr, validLocationFilePtr, errorFile);

	//Closes files
	fclose(fileLocationPtr);
	fclose(validLocationFilePtr);
	fclose(validBarcodeFilePtr);
	fclose(errorFile);

	return 0; 
}
