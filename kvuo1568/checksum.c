#include <stdio.h>

int main(int argc, char *argv[]){
	//Will contain the barcode to be checked
	char barcode[BUFSIZ];
	int i;	
	int finalTotal;
	FILE *fp;

	fp = fopen("errors.log", "a");

	if(fp == NULL)
	{
		perror("ERROR");
		return 1;
	}	

	//Reads the barcode line by line from the stdin 
	while(fgets(barcode,sizeof(barcode) , stdin) != NULL)
	{
		finalTotal = 0;
		for(i = 0; i < 11; i++)
		{
			//Adds the first 11 numbers to the total
			finalTotal +=  barcode[i] - '0';
		}

		//Checks if finalTotal equals the checksum provided by the barcode 
		if(finalTotal % 10 ==  barcode[11] - '0')
		{
			printf(barcode);
		}else
		{
			fprintf(fp, "Checksum is incorrect: %s", barcode);
		}
	}
	fclose(fp);
	return 0;
}
