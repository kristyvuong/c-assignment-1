#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{
	/*Given : barcode and location	
	 *shortLocation and barcodeLocation is used to compare the barcode and the given location without the null or new line characters 
	 */
	char barcode[BUFSIZ];
	char location[BUFSIZ];
	char shortLocation[5];
	char barcodeLocation[5];
	char *shortLocationPtr =  &shortLocation[0];
	char *barcodeLocationPtr = &barcodeLocation[0]; 
	FILE *fp;
	FILE *errorFile;

	// 0 = true, 1 = false
	int valid = 0;

	if(argc <  3)
	{
		printf("There are too few arguments provided \n");
		return 1;
	}

	//Make sure the right arguments are given 
	char *locations = "--locations";
	if(strcmp(argv[1],locations) != 0)
	{
		printf("You have bad arguments. Include --locations\n");
		return 1;
	}

	fp = fopen(argv[2],"r");
	errorFile = fopen("errors.log", "a");

	if(fp == NULL || errorFile == NULL)	
	{
		perror("ERROR");
		return 1;
	}

	while(fgets(barcode, sizeof(barcode), stdin) != NULL)
	{
		//Copies the first 4 chars (location) of barcode 
		strncpy(barcodeLocationPtr, barcode, 4);	

		while(fgets(location, sizeof(location), fp) != NULL)
		{
			//Copies the first 4 chars of location
			strncpy(shortLocationPtr,location,4);	

			//If the two strings are the same 
			if(strcmp(shortLocationPtr,barcodeLocationPtr) == 0)
			{
				printf(barcode);
				break;
			}else
			{valid = 1;
			}
		}
		if(valid == 1){				
			fprintf(errorFile, "Invalid location : %s", barcode);
		}	
		//Resets the file to be read again
		rewind(fp);
	}

	fclose(errorFile);
	fclose(fp);

	return 0;
}
