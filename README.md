This assignment is writing software that checks the train ticket validation. Every ticket has a barcode which is exactly 12 digits. The format of the barcode is as follows:

  *  the ﬁrst 4 digits is the location L
  * the next 7 digits is the ticket number N
  * the last digit is a checksum digit C

When it is presented to a program, it is in the form: LLLLNNNNNNNC

*Task 1: checksum.c*

* Check if the first 11 numbers equal to the checksum
	
*Task 2: check_location.c*
	
*  Check validity of the ticket using the location part of the barcode 
*  Uses external file (locations.txt)

*Task 3: validate_input.c*
	
* Check if the stdin data for barcodes are correct 

*Task 4: validate_ticket.c*
	
* Combines all previous tasks together
	
*Task 5: (added into the previous tasks)*
	
* Adds all errors into errors.log